import sys

outputFile = sys.argv[1]
input_list = sys.argv[2:]
mergedFile= open(outputFile, "a")

container = []

for file in input_list:
    f = open(file, 'r')
    for line in f:
        if line not in container:
            mergedFile.write(line)
            container.append(line)

mergedFile.close()

