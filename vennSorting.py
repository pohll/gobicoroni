import sys

S1DE = open("C:\\Users\\aschu\\Desktop\\Studium2.0\\gobicoroni\\venn\\all_mock_vs_all_S1_DE.txt", 'r')
S1DAS = open("C:\\Users\\aschu\\Desktop\\Studium2.0\\gobicoroni\\venn\\all_mock_vs_all_S1_DAS.txt", 'r')
S2DE = open("C:\\Users\\aschu\\Desktop\\Studium2.0\\gobicoroni\\venn\\all_mock_vs_all_S2_DE.txt", 'r')
S2DAS = open("C:\\Users\\aschu\\Desktop\\Studium2.0\\gobicoroni\\venn\\all_mock_vs_all_S2_DAS.txt", 'r')

S1DESet = []
for line in S1DE:
    S1DESet.append(line)
S1DESet = set(S1DESet)

S1DASSet = []
for line in S1DAS:
    S1DASSet.append(line)
S1DASSet = set(S1DASSet)


S2DESet = []
for line in S2DE:
    S2DESet.append(line)
S2DESet = set(S2DESet)


S2DASSet = []
for line in S2DAS:
    S2DASSet.append(line)
S2DASSet = set(S2DASSet)


S1DASSet = set(S1DASSet)
DESet = S1DESet.union(S2DESet)
DE1Set = DESet.union(S2DASSet)
DE2Set = DESet.union(S1DASSet)
DASUnion = S1DASSet.union(S2DASSet)
onlyS1DAS = S1DASSet.difference(DE1Set)
onlyS2DAS = S2DASSet.difference(DE2Set)
bothDAS = DASUnion.difference(DESet)
bothDAS = bothDAS.difference(onlyS1DAS.union(onlyS2DAS))
S2DEsmall = (S2DESet.difference(S1DASSet)).difference(S1DESet)
S2DASDEintersect = S2DASSet.intersection(S2DEsmall)



with open("C:\\Users\\aschu\\Desktop\\Studium2.0\\gobicoroni\\venn\\venn_diag_sets.txt", "w") as outputfile:
    outputfile.write("Genes only in S1DAS:\n")
    for entry in onlyS1DAS:
        outputfile.write(entry)
    outputfile.write("Genes only in S2DAS:\n")
    for entry in onlyS2DAS:
        outputfile.write(entry)
    outputfile.write("Genes in both DAS but not DE:\n")
    for entry in bothDAS:
        outputfile.write(entry)
    outputfile.write("Genes in intersection between S2 DAS and S2 DE:\n")
    for entry in S2DASDEintersect:
        outputfile.write(entry)
#mergedFile.write(entry)
#mergedFile.close()
