#!/usr/bin/env python3

# input: diffSplicing tsv file, output directory for plot, "diffExpr" or "diffSplicing"
# out: volcano plot
import pandas as pd
from bioinfokit import visuz
import sys

eventName = sys.argv[2] 
file = sys.argv[1]


df = pd.read_csv(file, delimiter="\t")

if eventName == "diffExp":
    # 1,2,3 col relevant
    pValIndex = "diffexp.fdr"
    fcIndex = "diffexp.log2fc"
elif eventName == "diffSplic":
    # 1,5,6 col relevant
    pValIndex = "diffsplic.fdr"
    fcIndex = "diffsplic.difflog2fc"

new = df[["gene", pValIndex, fcIndex]]
new.dropna(inplace=True)

new.reset_index(drop=True)

visuz.gene_exp.volcano(df=new, lfc=fcIndex, pv=pValIndex, color=("red", "grey", "blue"),
                       plotlegend=True,legendpos='upper right', legendanchor=(1.46,1), valpha=0.8, sign_line=True)



