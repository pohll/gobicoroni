#!/usr/bin/env python3

# input: gtf file, diffsplic.out file
# create dictionary: key: geneId, value: biotype
# output: tsv - cols: gene, biotype
import csv
import sys

gtf = sys.argv[1] # directory?
diffSplicOut = sys.argv[2]
outFile = sys.argv[3]
geneBiotypeDict = {}

geneId = None

with open(gtf,'r') as tsv:
    for line in csv.reader(tsv, delimiter='\t'):
        if not line[0].startswith("#") and line[2] == "exon":
            tempGeneId = line[8][line[8].index("gene_id")+9:
                                 line[8].index(";", line[8].index("gene_id")) - 1]
            if geneId is None or geneId!=tempGeneId:
                biotype = line[8][line[8].index("gene_biotype")+14:
                                 line[8].index(";", line[8].index("gene_biotype")) - 1]
                geneId = tempGeneId
                geneBiotypeDict[geneId]=biotype

f = open(outFile, "a")

f.write("gene" + "\t"+"biotype"+"\n")
with open(diffSplicOut,'r') as tsv:
    for line in csv.reader(tsv, delimiter='\t'):
        if line[0] in geneBiotypeDict:
            f.write(line[0]+"\t"+geneBiotypeDict[line[0]]+"\n")
f.close()





