#!/usr/bin/env python3

# input: gtf file, diffsplic.out file
# create dictionary: key: geneId, value: biotype
# output: tsv - cols: gene, biotype
import csv
import sys

gtf = sys.argv[1]
diffSplicOut = sys.argv[2]
outFile = sys.argv[3]
geneBiotypeDict = {}

geneId = None

with open(gtf,'r') as tsv:
    for line in csv.reader(tsv, delimiter='\t'):
        if not line[0].startswith("#") and line[2] == "exon":
            tempGeneId = line[8][line[8].index("gene_id")+9:
                                 line[8].index(";", line[8].index("gene_id")) - 1]
            if geneId is None or geneId!=tempGeneId:
                if "gene_biotype" in line[8]:
                    biotype = line[8][line[8].index("gene_biotype")+14:
                                     line[8].index(";", line[8].index("gene_biotype")) - 1]
                    if biotype == "" or biotype == "":
                        biotype = "protein_coding"
                    geneId = tempGeneId
                    geneBiotypeDict[geneId]=biotype

f = open(outFile, "a")

clusterDict = {"IG_V_gene": "IG", "IG_V_pseudogene": "pseudogene", "Mt_rRNA": "ncRNA","Mt_tRNA": "ncRNA","rRNA": "ncRNA",
               "ribozyme": "ncRNA","sRNA": "ncRNA","scaRNA":"ncRNA",
               "TR_V_pseudogene":"pseudogene", "lncRNA": "pseudogene", "miRNA": "ncRNA", "misc_RNA":"ncRNA",
               "polymorphic_pseudogene":"pseudogene", "processed_pseudogene":"pseudogene",
               "protein_coding":"protein_coding", "rRNA_pseudogene":"pseudogene","scRNA":"ncRNA",
               "scaRNA":"pseudogene", "snRNA":"ncRNA", "snoRNA":"ncRNA",
               "transcribed_processed_pseudogene":"pseudogene", "transcribed_unitary_pseudogene":"pseudogene",
               "transcribed_unprocessed_pseudogene":"pseudogene", "unitary_pseudogene":"pseudogene",
               "unprocessed_pseudogene":"pseudogene", "vaultRNA": "ncRNA", "TEC": "TEC", }

with open(diffSplicOut,'r') as tsv:
    for line in csv.reader(tsv, delimiter='\t'):
        header = '\t'.join(map(str, line))
        if line[1].startswith("gene"):
            f.write(header + "\t"+"biotype"+"\n")
        elif line[1] in geneBiotypeDict:
            if geneBiotypeDict[line[1]] in clusterDict:
                f.write(header+"\t"+clusterDict[geneBiotypeDict[line[1]]]+"\n")
            else:
                f.write(header + "\t" + geneBiotypeDict[line[1]] + "\n")
        else:
            f.write(header + "\t" + ""+"\n")

f.close()


