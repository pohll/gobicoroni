#!/usr/bin/env python3
# input: calu3_totalRNA_4h_untr_vs_12h_S1
# calu3_totalRNA_4h_untr_vs_12h_S2
# calu3_totalRNA_4h_untr_vs_24h_mock
# calu3_totalRNA_4h_untr_vs_24h_S1
# calu3_totalRNA_4h_untr_vs_24h_S2
# calu3_totalRNA_4h_untr_vs_4h_mock
# calu3_totalRNA_4h_untr_vs_4h_S1
# calu3_totalRNA_4h_untr_vs_4h_S2

import os
import sys
import pandas as pd

dirPath = sys.argv[1]
eventName = sys.argv[2] #"diffExpr" or "diffSplic"
outputPath = sys.argv[3]


if eventName == "diffExp":
    # 1,2,3 col relevant
    pValIndex = "diffexp.fdr"
    fcIndex = "diffexp.log2fc"
    df = pd.DataFrame(columns=['gene'])
elif eventName == "diffSplic":
    # 1,5,6 col relevant
    pValIndex = "diffsplic.fdr"
    fcIndex = "diffsplic.difflog2fc"
    df = pd.DataFrame(columns=["gene"])


for subDirs in os.listdir(dirPath):
    subDirName = os.fsdecode(subDirs)
    if subDirName.startswith("calu3_totalRNA_4h_untr"):
        name = subDirName[26:]
        pathToDiffOut = dirPath+"/"+subDirName+"/diff_splicing_outs/empire.diffsplic.outECC"
        tempDf = pd.read_csv(pathToDiffOut, delimiter="\t")
        diffDf = tempDf[["gene", pValIndex, fcIndex]]
        diffDf.columns=["gene", name+"_"+pValIndex, name+"_"+fcIndex]
        df = pd.merge(df, diffDf, on='gene', how='outer')

print(df.head())
df.to_csv(outputPath, sep="\t")
