import numpy as np
import sklearn as skl
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
import pandas as pd
import umap
import seaborn as sns
import sys

if len(sys.argv) >1:
    relevantGeneFile = sys.argv[1]
    readCountsFile = sys.argv[2]
    relevantgenes = []
    with open(relevantGeneFile, 'r') as f:
        for line in f:
            relevantgenes.append(line.strip())
    df = pd.read_csv(readCountsFile, sep="\t", low_memory=False)
else:
    relevantgenes = []
    '''with open("merged_mock_vs_S2_exp_and_splic.signif", 'r') as f:
        for line in f:
            relevantgenes.append(line.strip())'''
    df = pd.read_csv("C:\\Users\\aschu\\Desktop\\Studium2.0\\GoBI2020\\viral_sequences_annotations\\Calu3_totalRNA_readcounts.txt", sep="\t",low_memory=False)

'''for index, row in df.iterrows():
    gene_id = row['gene_id'][0:row['gene_id'].index(".")]
    if gene_id not in relevantgenes:
        df.drop(index, inplace=True)'''

df1 = df.reset_index(drop=True)

df1 = df
dfT = df1.transpose()
dfT.columns = dfT.iloc[0]
df1 = df1.drop("gene_id", axis=1)
dfT = dfT.drop(dfT.index[0])
array = ['S1','S1','S1','S1','S1','S1','S2','S2','S2','S2','S2','S2','mock','mock','mock','mock','untr','untr']
dfT.insert(0,"group",array,True)
dfT.group.value_counts()
dfT2 = dfT.drop("group", axis=1)
groups = ["S1", "S2", "mock","untr"]
#outlier aussortieren! sehr hohe reads schauen und dann cut off setzen
dfT2 = StandardScaler().fit_transform(dfT2)
dfT3 = df.drop(df.columns[0], axis=1)
df1 = StandardScaler().fit_transform(dfT3)


# Separating out the target
y = dfT.loc[:,['group']].values
# Standardizing the features
pca = PCA(n_components=2)
principalComponents = pca.fit_transform(dfT2)
principalDf = pd.DataFrame(data = principalComponents, columns = ['principal component 1', 'principal component 2'])
finalDf = pd.concat([principalDf, dfT[['group']]], axis = 1)
fig, ax = plt.subplots()
ax.set_xlabel('Principal Component 1', fontsize = 10)
ax.set_ylabel('Principal Component 2', fontsize = 10)
ax.set_title('2 component PCA', fontsize = 10)
colors = ['r', 'g', 'b']
plt.scatter(
    principalDf.loc[:,"principal component 1"],
    principalDf.loc[:,"principal component 2"],
    c = dfT.group.map({"S1": 0, "S2": 1, "mock": 2,"untr": 3}),
    cmap = 'Spectral',
    s = 40)
cbar = plt.colorbar(boundaries=np.arange(5)-0.5)
cbar.set_ticks(np.arange(4))
cbar.set_ticklabels(groups)
ax.grid()
plt.savefig("pca_unfiltered.jpg")

'''tsne = TSNE(n_components=2, verbose=0, perplexity=40, n_iter=300)
tsne_results = tsne.fit_transform(dfT2)
#dfT2['tsne-2d-one'] = tsne_results[:,0]
#dfT2['tsne-2d-two'] = tsne_results[:,1]
fig, ax = plt.subplots()
plt.scatter(
    tsne_results[:, 0],
    tsne_results[:, 1],
    c = dfT.group.map({"S1": 0, "S2": 1, "mock": 2,"untr": 3}),
    cmap='Spectral',
    s = 40
)
cbar = plt.colorbar(boundaries=np.arange(5)-0.5)
cbar.set_ticks(np.arange(4))
cbar.set_ticklabels(groups)
plt.title('TSNE projection of the ReadCounts', fontsize=10)
plt.savefig("tsne.jpg")'''


reducer = umap.UMAP()
embedding = reducer.fit_transform(dfT2)
fig, ax = plt.subplots()
plt.scatter(
    embedding[:, 0],
    embedding[:, 1],
    c=dfT.group.map({"S1": 0, "S2": 1, "mock": 2,"untr": 3}),
    cmap='Spectral',
    s=40)
plt.gca().set_aspect('equal', 'datalim')
cbar = plt.colorbar(boundaries=np.arange(5)-0.5)
cbar.set_ticks(np.arange(4))
cbar.set_ticklabels(groups)
annotations = ['4h','4h','12h','12h','24h','24h','4h','4h','12h','12h','24h','24h','4h','4h','24h','24h','4h','4h']
for x in range(len(annotations)):
    plt.annotate( annotations[x], xy=(embedding[x,0], embedding[x,1]), fontsize=8,
    textcoords = "offset points",
    xytext = (2, 6),
    ha = 'center')
#for i, label in enumerate(annotations):
    #plt.annotate(label,embedding[:,0][i], embedding[:,1][i])
#gene rausschmeißen, bei denen keine signifikante diffexp besteht
#plt.title('UMAP projection of al ReadCounts', fontsize=10)
plt.savefig("umap_nonfiltered1.jpg", dpi=600)

embedding2 = reducer.fit_transform(df1)
fig, ax = plt.subplots()
plt.scatter(
    embedding2[:, 0],
    embedding2[:, 1],
    #c = [sns.color_palette()[x] for x in dfT.group.map({"S1": 0, "S2": 1, "mock": 2,"untr": 3})],
    cmap='Spectral',
    s=10)
plt.gca().set_aspect('equal', 'datalim')
cbar = plt.colorbar(boundaries=np.arange(5)-0.5)
cbar.set_ticks(np.arange(4))
cbar.set_ticklabels(groups)
#gene rausschmeißen, bei denen keine signifikante diffexp besteht
#plt.title('UMAP projection of the ReadCounts', fontsize=10)
plt.savefig("umap_nonfiltered.jpg")