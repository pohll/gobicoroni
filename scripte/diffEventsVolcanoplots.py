#!/usr/bin/env python3

# input: diffSplicing tsv file, output directory for plot, "diffExpr" or "diffSplicing"
# out: volcano plot
import pandas as pd
from bioinfokit import visuz
import sys

eventName = sys.argv[2]
file = sys.argv[1]


df = pd.read_csv(file, delimiter="\t")

if eventName == "diffExp":
    # 1,2,3 col relevant
    pValIndex = "diffexp.fdr"
    fcIndex = "diffexp.log2fc"
elif eventName == "diffSplic":
    # 1,5,6 col relevant
    pValIndex = "diffsplic.fdr"
    fcIndex = "diffsplic.difflog2fc"

new = df[["gene", pValIndex, fcIndex]]
new.dropna(inplace=True)

new.reset_index(drop=True)
#new = new.fillna(0)
#new_row = {'gene':'dummy', pValIndex:0.005,fcIndex:2}
#new = new.append(new_row, ignore_index=True)

visuz.gene_exp.volcano(df=new, lfc=fcIndex, pv=pValIndex, color=("#ffc20a", "grey", "#0c7bdc"), pv_thr=(0.05, 0.05),
                       plotlegend=True,legendpos='upper right', legendanchor=(1.46,1), valpha=0.8, sign_line=True,
                       xlm=(-18,12,4), ylm=(0,15,2))





