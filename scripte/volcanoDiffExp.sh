#!/bin/bash
FILES=$1
pythonFile=$2

for f in $FILES/*
do
	if [ -d "$f" ];
	then
		echo "$f"
		cd $f
		python3 $pythonFile $f/diff_splicing_outs/empire.diffsplic.outECC diffExp
		mv volcano.png final_diffExpVolcanoPlot_merged.png
		cd ..
   	fi	
done
