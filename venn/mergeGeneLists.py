import sys

outputFile = sys.argv[1]
input_list = sys.argv[2:]
mergedFile= open(outputFile, "a")

container = list()

for file in input_list:
    f = open(file, 'r')
    for line in f:
        if line not in container:
            container.append(line)
        else:
            container.remove(line)

for entry in container:
    mergedFile.write(entry)
mergedFile.close()

